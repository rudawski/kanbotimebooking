﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KanBoApiExample.Model
{
    public class KanBoUser
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }

        public KanBoUser(int id, string title, string email)
        {
            Id = id;
            Title = title;
            Email = email;
        }
    }
}