﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace KanBoApiExample.Model
{
    public class KanBoBoard
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Group { get; set; }

        public string GroupPosition { get; set; }

        public bool IsLoaded { get; set; }

        public IList<KanBoList> Lists { get; set; }

        public IList<KanBoUser> Users { get; set; }

        public KanBoBoard(int id, string title)
        {
            Id = id;
            Title = title;
        }
    }

}