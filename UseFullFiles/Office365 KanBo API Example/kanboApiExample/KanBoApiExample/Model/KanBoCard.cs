﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KanBoApiExample.Model
{
    public class KanBoCard
    {
         public int Id { get; set; }       
        public string Title { get; set; }

        public KanBoCard(int id, string title)
        {
            Id = id;
            Title = title;
        }
    }
}