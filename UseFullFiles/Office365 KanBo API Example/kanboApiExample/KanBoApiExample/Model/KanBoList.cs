﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace KanBoApiExample.Model
{
    public class KanBoList
    {
        public int Id { get; set; }       
        public string Title { get; set; }

        public IList<KanBoCard> Cards { get; set; }

        public KanBoList(int id, string title)
        {
            Id = id;
            Title = title;
            Cards = new List<KanBoCard>();
        }
    }
}