﻿using KanBoApiExample.KanBo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KanBoApiExample
{
    /// <summary>
    /// This for is only example how to use KaBo WebAPI. Please change credentials and urls where KanBo is stored.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        KanboAuth _kanbo;
        private int _currentCardId;

        private async void Form1_Load(object sender, EventArgs e)
        {
            //url to your azure KanBo Web
            string kanboUrl = "https://yourcompany.azurewebsites.net";
            //url to your sharepoint 365 where KanBo app is installed
            string sharepointUrl = "https://yourcompany.sharepoint.com/sites/kanbo";            
            string login = "yourlogin@yourcomany.onmicrosoft.com";
            string password = "your password";
            _kanbo = new KanboAuth(sharepointUrl, kanboUrl, login, password);
            var boards = await _kanbo.GetBoards();
            _comboBoxBoard.DisplayMember = "Title";
            _comboBoxBoard.ValueMember = "Id";
            _comboBoxBoard.DataSource = boards;

            _comboBoxBoardAction.DisplayMember = "Title";
            _comboBoxBoardAction.ValueMember = "Id";
            _comboBoxBoardAction.DataSource = boards.ToList();

            _comboBoxList.DisplayMember = "Title";
            _comboBoxList.ValueMember = "Id";

            _comboBoxListAction.DisplayMember = "Title";
            _comboBoxListAction.ValueMember = "Id";

            _comboBoxUsers.DisplayMember = "Title";
            _comboBoxUsers.ValueMember = "Id";

        }

        private async void _comboBoxBoard_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedItemId = (int)_comboBoxBoard.SelectedValue;
            if (selectedItemId > 0)
            {

                var board = await _kanbo.GetBoard(selectedItemId);
                _comboBoxList.DataSource = board.Lists;
                _comboBoxUsers.DataSource = board.Users;
            }
            else
            {
                _comboBoxList.Items.Clear();
            }
        }

        private async void _buttonAddCard_Click(object sender, EventArgs e)
        {
            int selectedListId = (int)_comboBoxList.SelectedValue;            
            _currentCardId = await _kanbo.AddCard(selectedListId, _textBoxCardTitle.Text, 0);
            _groupBoxActions.Enabled = true;
            MessageBox.Show("OK");
        }

        private async void _buttonAddNote_Click(object sender, EventArgs e)
        {
            await _kanbo.AddNote(_currentCardId, _textBoxNote.Text, "Sample text");
            MessageBox.Show("OK");
        }

        private async void _buttonAddComment_Click(object sender, EventArgs e)
        {
            await _kanbo.AddComment(_currentCardId, _textBoxComment.Text);
            MessageBox.Show("OK");

        }

        private void _textBoxNote_TextChanged(object sender, EventArgs e)
        {

        }

        private async void _comboBoxBoardAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            _buttonMoveToList.Enabled = false;
            int selectedItemId = (int)_comboBoxBoardAction.SelectedValue;
            if (selectedItemId > 0)
            {

                var board = await _kanbo.GetBoard(selectedItemId);
                _comboBoxListAction.DataSource = board.Lists;
                _buttonMoveToList.Enabled = true;
            }
            else
            {
                _comboBoxListAction.Items.Clear();
            }
        }

        private async void _buttonMoveToList_Click(object sender, EventArgs e)
        {
            int selectedItemFromId = (int)_comboBoxList.SelectedValue;
            int selectedItemToId = (int)_comboBoxListAction.SelectedValue;

            await _kanbo.MoveCardToList(_currentCardId, selectedItemFromId, selectedItemToId,0);
            MessageBox.Show("OK");
            _groupBoxActions.Enabled = false;
        }

        private void _comboBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_comboBoxList.SelectedValue != null)
            {
                int selectedListId = (int)_comboBoxList.SelectedValue;
                _buttonAddCard.Enabled = true;
            }
            else
            {
                _buttonAddCard.Enabled = false;
            }

        }

        private async void _buttonAddUser_Click(object sender, EventArgs e)
        {
            int selectedItemId = (int)_comboBoxUsers.SelectedValue;
            await _kanbo.AddUserToCard(_currentCardId, selectedItemId);
            MessageBox.Show("OK");
        }
    }
}
