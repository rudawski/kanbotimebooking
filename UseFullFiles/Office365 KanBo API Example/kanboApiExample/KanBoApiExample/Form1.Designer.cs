﻿namespace KanBoApiExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._comboBoxBoard = new System.Windows.Forms.ComboBox();
            this._comboBoxList = new System.Windows.Forms.ComboBox();
            this._buttonAddCard = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxCardTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._groupBoxActions = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._comboBoxListAction = new System.Windows.Forms.ComboBox();
            this._buttonMoveToList = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._textBoxNote = new System.Windows.Forms.TextBox();
            this._buttonAddNote = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._textBoxComment = new System.Windows.Forms.TextBox();
            this._buttonAddComment = new System.Windows.Forms.Button();
            this._buttonAddUser = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this._comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._comboBoxBoardAction = new System.Windows.Forms.ComboBox();
            this._groupBoxActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // _comboBoxBoard
            // 
            this._comboBoxBoard.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxBoard.FormattingEnabled = true;
            this._comboBoxBoard.Location = new System.Drawing.Point(12, 27);
            this._comboBoxBoard.Name = "_comboBoxBoard";
            this._comboBoxBoard.Size = new System.Drawing.Size(292, 21);
            this._comboBoxBoard.TabIndex = 0;
            this._comboBoxBoard.SelectedIndexChanged += new System.EventHandler(this._comboBoxBoard_SelectedIndexChanged);
            // 
            // _comboBoxList
            // 
            this._comboBoxList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxList.FormattingEnabled = true;
            this._comboBoxList.Location = new System.Drawing.Point(12, 73);
            this._comboBoxList.Name = "_comboBoxList";
            this._comboBoxList.Size = new System.Drawing.Size(292, 21);
            this._comboBoxList.TabIndex = 1;
            this._comboBoxList.SelectedIndexChanged += new System.EventHandler(this._comboBoxList_SelectedIndexChanged);
            // 
            // _buttonAddCard
            // 
            this._buttonAddCard.Enabled = false;
            this._buttonAddCard.Location = new System.Drawing.Point(319, 113);
            this._buttonAddCard.Name = "_buttonAddCard";
            this._buttonAddCard.Size = new System.Drawing.Size(94, 23);
            this._buttonAddCard.TabIndex = 2;
            this._buttonAddCard.Text = "Add Card";
            this._buttonAddCard.UseVisualStyleBackColor = true;
            this._buttonAddCard.Click += new System.EventHandler(this._buttonAddCard_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select board:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select list:";
            // 
            // _textBoxCardTitle
            // 
            this._textBoxCardTitle.Location = new System.Drawing.Point(12, 115);
            this._textBoxCardTitle.Name = "_textBoxCardTitle";
            this._textBoxCardTitle.Size = new System.Drawing.Size(292, 20);
            this._textBoxCardTitle.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Card Title:";
            // 
            // _groupBoxActions
            // 
            this._groupBoxActions.Controls.Add(this.label8);
            this._groupBoxActions.Controls.Add(this._comboBoxBoardAction);
            this._groupBoxActions.Controls.Add(this._buttonAddUser);
            this._groupBoxActions.Controls.Add(this.label7);
            this._groupBoxActions.Controls.Add(this._comboBoxUsers);
            this._groupBoxActions.Controls.Add(this.label6);
            this._groupBoxActions.Controls.Add(this._textBoxComment);
            this._groupBoxActions.Controls.Add(this._buttonAddComment);
            this._groupBoxActions.Controls.Add(this.label5);
            this._groupBoxActions.Controls.Add(this._textBoxNote);
            this._groupBoxActions.Controls.Add(this._buttonAddNote);
            this._groupBoxActions.Controls.Add(this._buttonMoveToList);
            this._groupBoxActions.Controls.Add(this.label4);
            this._groupBoxActions.Controls.Add(this._comboBoxListAction);
            this._groupBoxActions.Enabled = false;
            this._groupBoxActions.Location = new System.Drawing.Point(12, 166);
            this._groupBoxActions.Name = "_groupBoxActions";
            this._groupBoxActions.Size = new System.Drawing.Size(561, 222);
            this._groupBoxActions.TabIndex = 7;
            this._groupBoxActions.TabStop = false;
            this._groupBoxActions.Text = "Card Actions";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(184, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Lists:";
            // 
            // _comboBoxListAction
            // 
            this._comboBoxListAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxListAction.FormattingEnabled = true;
            this._comboBoxListAction.Location = new System.Drawing.Point(187, 45);
            this._comboBoxListAction.Name = "_comboBoxListAction";
            this._comboBoxListAction.Size = new System.Drawing.Size(156, 21);
            this._comboBoxListAction.TabIndex = 5;
            // 
            // _buttonMoveToList
            // 
            this._buttonMoveToList.Location = new System.Drawing.Point(361, 43);
            this._buttonMoveToList.Name = "_buttonMoveToList";
            this._buttonMoveToList.Size = new System.Drawing.Size(99, 23);
            this._buttonMoveToList.TabIndex = 7;
            this._buttonMoveToList.Text = "Move To List";
            this._buttonMoveToList.UseVisualStyleBackColor = true;
            this._buttonMoveToList.Click += new System.EventHandler(this._buttonMoveToList_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Note:";
            // 
            // _textBoxNote
            // 
            this._textBoxNote.Location = new System.Drawing.Point(24, 96);
            this._textBoxNote.Name = "_textBoxNote";
            this._textBoxNote.Size = new System.Drawing.Size(319, 20);
            this._textBoxNote.TabIndex = 9;
            this._textBoxNote.TextChanged += new System.EventHandler(this._textBoxNote_TextChanged);
            // 
            // _buttonAddNote
            // 
            this._buttonAddNote.Location = new System.Drawing.Point(361, 93);
            this._buttonAddNote.Name = "_buttonAddNote";
            this._buttonAddNote.Size = new System.Drawing.Size(94, 23);
            this._buttonAddNote.TabIndex = 8;
            this._buttonAddNote.Text = "Add note";
            this._buttonAddNote.UseVisualStyleBackColor = true;
            this._buttonAddNote.Click += new System.EventHandler(this._buttonAddNote_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Comment:";
            // 
            // _textBoxComment
            // 
            this._textBoxComment.Location = new System.Drawing.Point(24, 142);
            this._textBoxComment.Name = "_textBoxComment";
            this._textBoxComment.Size = new System.Drawing.Size(319, 20);
            this._textBoxComment.TabIndex = 12;
            // 
            // _buttonAddComment
            // 
            this._buttonAddComment.Location = new System.Drawing.Point(361, 139);
            this._buttonAddComment.Name = "_buttonAddComment";
            this._buttonAddComment.Size = new System.Drawing.Size(94, 23);
            this._buttonAddComment.TabIndex = 11;
            this._buttonAddComment.Text = "Add comment";
            this._buttonAddComment.UseVisualStyleBackColor = true;
            this._buttonAddComment.Click += new System.EventHandler(this._buttonAddComment_Click);
            // 
            // _buttonAddUser
            // 
            this._buttonAddUser.Location = new System.Drawing.Point(361, 184);
            this._buttonAddUser.Name = "_buttonAddUser";
            this._buttonAddUser.Size = new System.Drawing.Size(99, 23);
            this._buttonAddUser.TabIndex = 16;
            this._buttonAddUser.Text = "Add user";
            this._buttonAddUser.UseVisualStyleBackColor = true;
            this._buttonAddUser.Click += new System.EventHandler(this._buttonAddUser_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Users:";
            // 
            // _comboBoxUsers
            // 
            this._comboBoxUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUsers.FormattingEnabled = true;
            this._comboBoxUsers.Location = new System.Drawing.Point(24, 184);
            this._comboBoxUsers.Name = "_comboBoxUsers";
            this._comboBoxUsers.Size = new System.Drawing.Size(319, 21);
            this._comboBoxUsers.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Boards:";
            // 
            // _comboBoxBoardAction
            // 
            this._comboBoxBoardAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxBoardAction.FormattingEnabled = true;
            this._comboBoxBoardAction.Location = new System.Drawing.Point(24, 45);
            this._comboBoxBoardAction.Name = "_comboBoxBoardAction";
            this._comboBoxBoardAction.Size = new System.Drawing.Size(156, 21);
            this._comboBoxBoardAction.TabIndex = 17;
            this._comboBoxBoardAction.SelectedIndexChanged += new System.EventHandler(this._comboBoxBoardAction_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 409);
            this.Controls.Add(this._groupBoxActions);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._textBoxCardTitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._buttonAddCard);
            this.Controls.Add(this._comboBoxList);
            this.Controls.Add(this._comboBoxBoard);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this._groupBoxActions.ResumeLayout(false);
            this._groupBoxActions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _comboBoxBoard;
        private System.Windows.Forms.ComboBox _comboBoxList;
        private System.Windows.Forms.Button _buttonAddCard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxCardTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox _groupBoxActions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox _comboBoxListAction;
        private System.Windows.Forms.Button _buttonAddUser;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _comboBoxUsers;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _textBoxComment;
        private System.Windows.Forms.Button _buttonAddComment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _textBoxNote;
        private System.Windows.Forms.Button _buttonAddNote;
        private System.Windows.Forms.Button _buttonMoveToList;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _comboBoxBoardAction;
    }
}

