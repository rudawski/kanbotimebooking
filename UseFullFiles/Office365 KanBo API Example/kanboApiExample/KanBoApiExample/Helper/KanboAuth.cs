﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using KanBoApiExample.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace KanBoApiExample.KanBo
{
    public class KanboAuth
    {
        //this member contains authentication to KanBo
        private CapsuleAuthentication _auth;
        private Uri _kanboUri;

        /// <summary>
        /// initializes KanBo connection
        /// </summary>
        /// <param name="sharepointUrl"></param>
        /// <param name="kanboUrl"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public KanboAuth(string sharepointUrl, string kanboUrl, string login, string password)
        {
            _kanboUri = new Uri(kanboUrl);
            object auth;
            var requestTokenUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/RequestToken");
            var request = WebRequest.Create(requestTokenUrl);
            request.Method = "POST";

            var param = new
            {
                Url = sharepointUrl,
                User = login,
                Password = password
            };

            var jsonString = new
            {
                Type = "CloudCredentialsAuth",
                //Type = "AcsAuthParams",
                Str = JsonConvert.SerializeObject(param)
            };

            var jsonData = JsonConvert.SerializeObject(jsonString);
            var byteArray = Encoding.UTF8.GetBytes(jsonData);

            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var response = request.GetResponse();
            dataStream = response.GetResponseStream();
            if (dataStream != null)
            {
                var reader = new StreamReader(dataStream);
                var responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
                response.Close();

                auth = JsonConvert.DeserializeObject<CapsuleAuthentication>(responseFromServer);
                _auth = (CapsuleAuthentication)auth;
            }
        }


        /// <summary>
        /// Creates card with specified title
        /// </summary>
        /// <param name="listId">To which list card should be added</param>
        /// <param name="topic">Card title</param>
        /// <param name="position">Position where card should be placed - 0 means on the top</param>
        /// <returns>returns card id</returns>
        public async Task<int> AddCard(int listId, string topic, int position)
        {
            int result = 0;
            var requestUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

            //parameters required for AddCard.
            var param = new
            {
                Name = topic,
                ListId = listId,
                Position = position
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1, //always 1
                MethodType = "Action", //type of action (Get or Set)
                Method = "AddCard", //method
                Data = JsonConvert.SerializeObject(param)
            };

            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUrl, content);
            string responseFromServer = await response.Content.ReadAsStringAsync();

            //parse response after execution
            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                //analyze token after not successful execution
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message").);
            }
            //request has been executed successfully
            var dataResp = resp.SelectToken("Data").ToString();
            if (!string.IsNullOrEmpty(dataResp))
            {
                //we can read returned data
                var getDataResp = JObject.Parse(dataResp);
                var cardId = getDataResp["Id"];

                result = (int)cardId;
            }
            return result;
        }

        /// <summary>
        /// Adds comment to card
        /// </summary>
        /// <param name="cardId">Card id where comment should be added</param>
        /// <param name="message">Text of message</param>
        /// <returns></returns>
        public async Task AddComment(int cardId, string message)
        {
            int result = 0;

            var requestUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

            var param = new
            {
                CardId = cardId,
                Message = message
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "Action",
                Method = "AddCommentOnCard",
                Data = JsonConvert.SerializeObject(param)
            };

            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUrl, content);
            string responseFromServer = await response.Content.ReadAsStringAsync();

            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message"));
            }
        }

        /// <summary>
        /// Moves card to specified list
        /// </summary>
        /// <param name="cardId">Card id which will be moved</param>
        /// <param name="listFromId">Source list id</param>
        /// <param name="listToId">Destination list id</param>
        /// <param name="position">New Card position on the destionation list</param>
        /// <returns></returns>
        public async Task MoveCardToList(int cardId, int listFromId, int listToId, int position)
        {
            int result = 0;

            var requestUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

            var param = new
            {
                Id = cardId,
                ContainerFromId = listFromId,
                ContainerToId = listToId,
                Position = position
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "Action",
                Method = "ChangeCardParent",
                Data = JsonConvert.SerializeObject(param)
            };

            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUrl, content);
            string responseFromServer = await response.Content.ReadAsStringAsync();

            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message"));
            }
        }

        /// <summary>
        /// Adds note to the card
        /// </summary>
        /// <param name="cardId">Note will be added to this card</param>
        /// <param name="name">Name of added note</param>
        /// <param name="body">Body of added note</param>
        /// <returns>id of created note</returns>
        public async Task<int> AddNote(int cardId, string name, string body)
        {
            int result = 0;

            var requestUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

            var param = new
            {
                Name = name,
                CardId = cardId,
                Text = body,
                Position = 0
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "Action",
                Method = "AddNote",
                Data = JsonConvert.SerializeObject(param)
            };

            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUrl, content);
            string responseFromServer = await response.Content.ReadAsStringAsync();




            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                //throw new Exception(msg.SelectToken("Message"));
            }

            var dataResp = resp.SelectToken("Data").ToString();

            if (!string.IsNullOrEmpty(dataResp))
            {
                var getDataResp = JObject.Parse(dataResp);
                var noteId = getDataResp["Id"];

                result = (int)noteId;
                //add note

            }
            return result;
        }

        /// <summary>
        /// Adds user to card
        /// </summary>
        /// <param name="cardId">User will be added to card with this id</param>
        /// <param name="userId">User id which we want to add to the card</param>
        /// <returns></returns>
        public async Task AddUserToCard(int cardId, int userId)
        {
            if (userId > 0)
            {
                var requestUrl = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

                var param = new
                {
                    CardId = cardId,
                    UserId = userId
                };

                var jsonString = new
                {
                    Auth = _auth,
                    Id = 1,
                    MethodType = "Action",
                    Method = "AddUserToCard",
                    Data = JsonConvert.SerializeObject(param)
                };

                var jsonData = JsonConvert.SerializeObject(jsonString);
                StringContent content = new StringContent(jsonData);

                HttpResponseMessage response = await Post(requestUrl, content);
                string responseFromServer = await response.Content.ReadAsStringAsync();

                var resp = JObject.Parse(responseFromServer);
                if (!(bool)resp.SelectToken("Success"))
                {
                    var msg = resp.SelectToken("Exception");
                    throw new Exception("Error");
                }
            }

        }


        /// <summary>
        /// Gets board assigned to user
        /// </summary>
        /// <returns>List of available boards for user</returns>
        public async Task<List<KanBoBoard>> GetBoards()
        {
            List<KanBoBoard> result = new List<KanBoBoard>();

            var param = new
            {
                Id = 1
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "GetData",
                Method = "ViewLandingPage",
                Data = JsonConvert.SerializeObject(param)
            };

            var requestUri = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");
            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUri.ToString(), content);
            string responseFromServer = await response.Content.ReadAsStringAsync();

            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message"));
            }
            var id = resp.SelectToken("RequestId").ToString();

            var dataResp = resp.SelectToken("Data").ToString();

            if (!string.IsNullOrEmpty(dataResp))
            {
                var getDataResp = JObject.Parse(dataResp);

                foreach (var boardGroup in getDataResp.SelectToken("BoardsGroups").OrderBy(p => p["Position"]))
                {

                    var names = from g in boardGroup["Boards"]
                                orderby g["Name"]
                                select g;
                    if (names.Any())
                    {
                        result.Add(new KanBoBoard(-1, string.Format("--{0}--", boardGroup["Name"])));
                        foreach (var token in names)
                        {
                            var boardid = (int)token["Id"];
                            var boardName = EscapeQuotes(token["Name"].ToString());
                            result.Add(new KanBoBoard(boardid, boardName));
                        }
                    }

                }
            }



            return result;
        }

        /// <summary>
        /// Gets board by board Id
        /// </summary>
        /// <param name="boardId"></param>
        /// <returns>Board with assigned Lists and Users</returns>
        public async Task<KanBoBoard> GetBoard(int boardId)
        {
            KanBoBoard result = null;

            var param = new
            {
                Id = boardId
            };

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "GetData",
                Method = "ViewBoard",
                Data = JsonConvert.SerializeObject(param)
            };

            var users = await GetUsers();
            var requestUri = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");
            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUri.ToString(), content);
            string responseFromServer = await response.Content.ReadAsStringAsync();


            var resp = JObject.Parse(responseFromServer);
            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message").ToString());
            }
            var dataResp = resp.SelectToken("Data").ToString();
            if (!string.IsNullOrEmpty(dataResp))
            {
                var getDataResp = JObject.Parse(dataResp);

                string boardName = getDataResp["Name"].ToString();


                var userTokens = from g in getDataResp.SelectTokens("Groups") select g;

                var userids = userTokens.Select(userToken =>
                {
                    var uT = userToken.Value<JObject>();
                    uT.Remove("ElementName");
                    return uT;
                })
                    .SelectMany(userTokenObject => userTokenObject.Properties()).
                    Select(property => property.Value.Value<JArray>())
                    .SelectMany(array => array.Children<JObject>())
                    .Select(x => x.Property("UserIds").Value.Value<JArray>())
                    .SelectMany(array => array.Children<JValue>())
                    .Select(jvalue => jvalue.Value);

                //.Select(array => array.Values<int>()).ToList();

                result = new KanBoBoard(boardId, boardName);
                var boardUsers = users.Where(x => userids.Contains((long)x.Id));
                result.Users = boardUsers.ToList<KanBoUser>();

                var names = from list in getDataResp.SelectToken("Lists")
                            select list;
                List<KanBoList> lists = new List<KanBoList>();

                foreach (var token in names)
                {
                    var listid = (int)token["Id"];
                    var listname = token["Name"].ToString();
                    lists.Add(new KanBoList(listid, listname));
                }
                result.Lists = lists;

            }

            return result;

        }

        /// <summary>
        /// Gets users assigned to KanBo
        /// </summary>
        /// <returns></returns>
        private async Task<List<KanBoUser>> GetUsers()
        {
            List<KanBoUser> result = new List<KanBoUser>();


            var requestUri = string.Concat(_kanboUri.Scheme, "://", _kanboUri.Host, "/JsonApi");

            var jsonString = new
            {
                Auth = _auth,
                Id = 1,
                MethodType = "GetData",
                Method = "Users",
                Data = "{\"Id\":1}"
            };


            var jsonData = JsonConvert.SerializeObject(jsonString);
            StringContent content = new StringContent(jsonData);

            HttpResponseMessage response = await Post(requestUri.ToString(), content);
            string responseFromServer = await response.Content.ReadAsStringAsync();


            var resp = JObject.Parse(responseFromServer);


            if (!(bool)resp.SelectToken("Success"))
            {
                var msg = resp.SelectToken("Exception");
                throw new Exception("Error");
                //throw new Exception(msg.SelectToken("Message"));
            }
            var id = resp.SelectToken("RequestId").ToString();

            var dataResp = resp.SelectToken("Data").ToString();

            if (!string.IsNullOrEmpty(dataResp))
            {
                var getDataResp = JObject.Parse(dataResp);

                var names = from users in getDataResp.SelectToken("Users")
                            select users;

                foreach (var token in names)
                {
                    var userid = (int)token["Id"];
                    var userName = token["Name"].ToString();
                    var email = token["Email"].ToString();
                    result.Add(new KanBoUser(userid, userName, email));
                }
            }
            return new List<KanBoUser>(result.OrderBy(p => p.Title));
        }


        private string EscapeQuotes(string text)
        {
            text = text.Replace("'", "");
            return text;
        }

        /// <summary>
        /// invokes a post request
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> Post(string requestUri, StringContent requestData)
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, requestUri);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            requestData.Headers.ContentType = MediaTypeHeaderValue.Parse("application/atom+xml");
            request.Content = requestData;
            return await client.SendAsync(request);
        }


        public class CapsuleAuthentication
        {
            public string Type { get; set; }
            public string Str { get; set; }
        }

    }
}