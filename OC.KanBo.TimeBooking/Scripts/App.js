﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var customerList = context.get_web().get_lists().getByTitle('CustomerList');
var customerItems = new Object();

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    fillWithTestData();
    getUserName();
    //getCustomers();
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text('Hello ' + user.get_title());
    getCustomers();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function fillWithTestData()
{
    var ddlBoard = getControlByPartialId('ddlBoard');
    var item = document.createElement("option");
    item.textContent = '';
    item.value = '';
    ddlBoard.options.add(item);

    item.textContent = 'Board 1';
    item.value = 'http://google1.pl';
    ddlBoard.options.add(item);

    item = document.createElement("option");
    item.textContent = 'Board 2';
    item.value = 'http://google2.pl';
    ddlBoard.options.add(item);

    item = document.createElement("option");
    item.textContent = 'Board 3';
    item.value = 'http://google3.pl';
    ddlBoard.options.add(item);



    var ddlCard = getControlByPartialId('ddlCard');
    item = document.createElement("option");
    item.textContent = '';
    item.value = '';
    ddlCard.options.add(item);

    item.textContent = 'Card 1';
    item.value = 'http://interia1.pl';
    ddlCard.options.add(item);

    item = document.createElement("option");
    item.textContent = 'Card 2';
    item.value = 'http://interia2.pl';
    ddlCard.options.add(item);

    item = document.createElement("option");
    item.textContent = 'Card 3';
    item.value = 'http://interia3.pl';
    ddlCard.options.add(item);
}

function getCustomers() {
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml('<View><Query><Where><Geq><FieldRef Name=\'ID\'/><Value Type=\'Number\'>0</Value></Geg></Where></Query></View>');
    customerItems = customerList.getItems(camlQuery);
    context.load(customerItems);
    context.executeQueryAsync(onGetCutomerItemsSuccess, onGetCustomerItemsFail)
    //context.executeQueryAsync(Function.createDelegate(this, this.onGetCutomerItemsSuccess), Function.createDelegate(this, this.onGetCustomerItemsFail))
}

function onGetCutomerItemsSuccess() {
    if (customerItems) {
        var itemsEnumerator = customerItems.getEnumerator();
        var ddlCustomerElement = getControlByPartialId('ddlCustomer');
        if (ddlCustomerElement != null && ddlCustomerElement != NaN) {
            var item = document.createElement("option");
            item.textContent = '';
            item.value = 0;
            ddlCustomerElement.options.add(item);

            while (itemsEnumerator.moveNext()) {
                var dllItem = document.createElement("option");
                var spItem = itemsEnumerator.get_current();
                dllItem.textContent = spItem.get_item('Title');
                dllItem.value = spItem.get_item('ID');
                ddlCustomerElement.options.add(dllItem);
            }
        }
    }
}

function onGetCustomerItemsFail(sender, args) {
    alert('Failed to get Customers. Error:' + args.get_message());
}


function getControlByPartialId(srchId)
{
    var jquerySearch = $('[id*=\'' + srchId + '\']');
    if (jquerySearch != null && jquerySearch.length > 0)
    {
        return jquerySearch[0];
    }

    return null;
}


function OnButtonSaveClick(sender, args) {
    if (Page_ClientValidate())
    {
        var context1 = SP.ClientContext.get_current();
        var timeBookingList = context1.get_web().get_lists().getByTitle('TimeBookingList');
        var itemCreateInfo = new SP.ListItemCreationInformation();
        var newListItem = timeBookingList.addItem(itemCreateInfo);

        var control = getControlByPartialId('tbShortDescr');
        var shortDescr = control.value;

        control = getControlByPartialId('ddlBoard');
        var urlBoardValue = new SP.FieldUrlValue();
        urlBoardValue.set_url(control.value);
        urlBoardValue.set_description(control.item(control.selectedIndex).textContent);

        control = getControlByPartialId('ddlCard');
        var urlCardValue = new SP.FieldUrlValue();
        urlCardValue.set_url(control.value);
        urlCardValue.set_description(control.item(control.selectedIndex).textContent);

        var employeeValue = new SP.FieldUserValue();
        employeeValue.set_lookupId(context.get_web().get_currentUser().get_id());

        control = getControlByPartialId('dtDate');
        var workDay = control.value;

        control = getControlByPartialId('tbHours');
        var workHours = control.value;

        control = getControlByPartialId('ddlCustomer');
        var customerName = control.item(control.selectedIndex).textContent;

        control = getControlByPartialId('tbComments');
        var comments = control.value;

        //urlCardValue.set_description('Ala ma kota');
        //urlCardValue.set_url('http://www.google.pl');
        //urlBoardValue.set_description('Ala ma psa');
        //urlBoardValue.set_url('http://www.google.de');

        newListItem.set_item('ContentTypeId', '0x01004F8F6871E2C141FBA53BEA19015253AE');
        newListItem.set_item('Title', shortDescr);
        newListItem.set_item('Card', urlCardValue);
        newListItem.set_item('Board', urlBoardValue);
        newListItem.set_item('WorkDay', workDay);
        newListItem.set_item('WorkHours', workHours);
        //newListItem.set_item('_Comments', comments);
        newListItem.set_item('Comments', comments);
        newListItem.set_item('Customer', customerName);
        newListItem.set_item('Employee', employeeValue);

        newListItem.update();

        context1.load(newListItem);

        context1.executeQueryAsync(onCreateItemSuccess, onCreateItemFail)
        //context.executeQueryAsync(Function.createDelegate(this, this.onCreateItemSuccess), Function.createDelegate(this, this.onCreateItemFail));
    }
}

function OnButtonCancelClick() {
    clearFields();

    //getAllHostWebPropsUsingREST();

    //getHostInfo();

    //getKanboUsingREST();

    getKanboUsingWebRequest();
}

function onCreateItemSuccess(sender, args) {
    alert('Failed to get Customers. Error:' + args.get_message());
}

function onCreateItemFail(sender, args) {
    alert('Failed to get Customers. Error:' + args.get_message());
}



function validateNumeric(source, args) {
    args.IsValid = !isNaN(parseFloat(args.Value)) && isFinite(args.Value) && args.Value > 0;
}

function validateSelectedBoard(source, args) {
    args.IsValid = args.Value;
}

function validateSelectedCard(source, args) {
    args.IsValid = args.Value;
}

function clearFields() {
    var control = getControlByPartialId('tbShortDescr');
    control.value = '';

    control = getControlByPartialId('ddlBoard');
    control.selectedIndex = 0;

    control = getControlByPartialId('ddlCard');
    control.selectedIndex = 0;

    control = getControlByPartialId('dtDate');
    control.value = '';

    control = getControlByPartialId('tbHours');
    control.value = '';

    control = getControlByPartialId('ddlCustomer');
    control.selectedIndex = 0;

    control = getControlByPartialId('tbComments');
    control.value = '';
}

// ************************************************************************************************************************

function getKanboUsingWebRequest()
{
    var kanboUri = new URI('https://kompus-kanbo.azurewebsites.net');
    var kanboUrl = 'https://kompus-kanbo.azurewebsites.net';
    var kanboSiteUri = new URI('https://objconn.sharepoint.com');
    var kanboSiteUrl = 'https://objconn.sharepoint.com';
    var requestToken = 'https://kompus-kanbo.azurewebsites.net/RequestToken';
    var request = new SP.WebRequestInfo();

    var executor = new SP.RequestExecutor(kanboUrl);

    var strObj = {
        Url: kanboSiteUrl,
        User: 'pr@ObjConn.onmicrosoft.com',
        Password: 'Monisia101'
    }

    var dataObj = {
        Type: 'CloudCredentialsAuth',
        Str: JSON.stringify(strObj),
    }

    var jasonString = JSON.stringify(dataObj);

    executor.executeAsync(
        {
            url: kanboUrl,
            type: 'POST',
            username: 'pr@ObjConn.onmicrosoft.com',
            password: 'Monisia101',
            data: dataObj,
            success: onGetKanboUsingWebRequestSuccess,
            error: onGetKanboUsingWebRequestError
        }
    );

    function onGetKanboUsingWebRequestSuccess(data) {
        $('<div>' + 'getKanboUsingREST() - Title: ' + web.get_title() + '\n' + '</div>'
        ).appendTo('.TimeBookingContent');
    }

    function onGetKanboUsingWebRequestError(data, errorCode, errorMessage) {
        $('<div>' + 'getKanboUsingREST() - StatusCode: ' + statusCode + '\n' +
        'Message: ' + errorMessage + '</div>'
        ).appendTo('.TimeBookingContent');
    }
}

function getKanboUsingREST()
{
    var context = new SP.ClientContext('https://kompus-kanbo.azurewebsites.net');
    //Get the ProxyWebRequestExecutorFactory
    var factory = new SP.ProxyWebRequestExecutorFactory('https://kompus-kanbo.azurewebsites.net');
    //Assign the factory to the client context.
    context.set_webRequestExecutorFactory(factory);
    var web = context.get_web();
    //Load Web.
    context.load(web);
    context.executeQueryAsync(successHandlerCSOM, errorHandlerCSOM);

    function successHandlerCSOM(data) {
        $('<div>' + 'getKanboUsingREST() - Title: ' + web.get_title() + '\n' + '</div>'
        ).appendTo('.TimeBookingContent');
    }

    function errorHandlerCSOM(data, errorCode, errorMessage) {
        $('<div>' + 'getKanboUsingREST() - StatusCode: ' + statusCode + '\n' +
        'Message: ' + errorMessage + '</div>'
        ).appendTo('.TimeBookingContent');
    }

}

function getAllHostWebPropsUsingREST() {
    var executor;

    var hostweburl = decodeURIComponent($.getUrlVar("SPHostUrl"));
    var appweburl = decodeURIComponent($.getUrlVar("SPAppWebUrl"));

    // although we're fetching data from the host web, SP.RequestExecutor gets initialized with the app web URL..
    //executor = new SP.RequestExecutor(appweburl);

    //executor.executeAsync(
    //    {
    //        url:
    //            appweburl +
    //            "/_api/SP.AppContextSite(@target)/web/?@target='" + hostweburl + "'",
    //        method: "GET",
    //        headers: { "Accept": "application/json; odata=verbose" },
    //        success: onGetAllHostWebPropsUsingRESTSuccess,
    //        error: onGetAllHostWebPropsUsingRESTFail
    //    }
    //);


    var context = SP.ClientContext.get_current();
    var request = new SP.WebRequestInfo();
    //request.set_url("https://canbo.it/JsonApi");
    //request.set_url("https://ockanbo.azurewebsites.net/intranet/JsonApi")
    //request.set_url("https://ockanbo.azurewebsites.net/intranet/2/handshake")

    //request.set_url("https://ockanbo.azurewebsites.net/intranet/2/handshake")
    //request.set_method('POST');
    ////request.set_body('{"Auth":{"Type":"SessionParams","Str":"token"},"Id":1,"MethodType":"GetData","Data":"{\"Id\":27}","Method":"ViewBoard"}');
    ////request.set_body('{"Auth":{"Type":"SessionParams"},"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    ////request.set_body('{"Auth":{"Type":"SessionParams", Str:"{\"Url\":\"https://ockanbo.azurewebsites.net/intranet", \"User\":\"admin@canbo.it\",\"Password\":\"Admin!@#$\"}"},"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    //request.set_body('{"Type":"SessionParams","Str":"{\"Url\":\"https://ockanbo.azurewebsites.net/intranet/1\", \"User\":\"admin@canbo.it\",\"Password\":\"Admin!@#$\"}"}');


    //request.set_url("https://ockanbo.azurewebsites.net/intranet/2");
    //request.set_url("https://ockanbo.azurewebsites.net/JsonApi");
    request.set_url("https://kompus-kanbo.azurewebsites.net/");
    request.set_method('POST');
    request.set_headers('{ "Accept": "application/json" }');
    //request.set_headers('{ "Accept": "application/json;odata=verbose" }')
    //request.set_body('{"Auth":{"Type":"SessionParams","Str":"token"},"Id":1,"MethodType":"GetData","Data":"{\"Id\":27}","Method":"ViewBoard"}');
    //request.set_body('{"Auth":{"Type":"SessionParams"},"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    //request.set_body('{"Auth":{"Type":"SessionParams", Str:"{\"Url\":\"https://ockanbo.azurewebsites.net/intranet/1", \"User\":\"admin@canbo.it\",\"Password\":\"Admin!@#$\"}"},"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    //request.set_body('{"Type":"SessionParams","Str":"{\"Url\":\"https://ockanbo.azurewebsites.net/intranet/2", \"User\":\"dziembor@objectconnect.de\",\"Password\":\"haslo\"}"}');
    //request.set_body('{"Type":"SessionParams","Str":"{\"Url\":\"https://ockanbo.azurewebsites.net/intranet/2", \"User\":\"dziembor@objectconnect.de\",\"Password\":\"haslo\"}"' +
    //                 ',"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    //request.set_body('{"Type":"SessionParams","Str":"{\"Url\":\"https://ockanbo.azurewebsites.net/JsonApi", \"User\":\"dziembor@objectconnect.de\",\"Password\":\"haslo\"}"' +
    //                 ',"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');
    request.set_body('{"Type":"SessionParams","Str":"{\"Url\":\"https://kompus-kanbo.azurewebsites.net/", \"User\":\"dziembor@objectconnect.de\",\"Password\":\"haslo\"}"' +
                 ',"Id":1,"MethodType":"GetData","Method":"GetDataMethods"}');

    var response = SP.WebProxy.invoke(context, request);
    context.executeQueryAsync(WebTokenCallSuccess, WebTokenCallError);

    function WebTokenCallSuccess()
    {
        var statusCode = response.get_statusCode();
        var body = response.get_body();

            $('<div>' + 'StatusCode: ' + statusCode + '\n' +
            'Body: ' + body + '</div>'
            ).appendTo('.TimeBookingContent');
    }

    function WebTokenCallError()
    {

    }

    function WebCallSuccess()
    {

    }

    function WebCallError()
    {

    }

}

function onGetAllHostWebPropsUsingRESTSuccess(data) {
    var jsonObject = JSON.parse(data.body);
    // note that here our jsonObject.d object (representing the web) has ALL properties because 
    // we did not select specific ones in our REST URL. However, we're just displaying the web title here..

    var control = getControlByPartialId('tbComments');
    control.value = jsonObject.d.Title;
    //$('#hostWebTitle').text(jsonObject.d.Title);
}

function onGetAllHostWebPropsUsingRESTFail(data, errorCode, errorMessage) {
    alert('Failed to get host site. Error:' + errorMessage);
}


// jQuery plugin for fetching querystring parameters..
jQuery.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return jQuery.getUrlVars()[name];
    }
});


//********************************************************************************************************************

function execCrossDomainRequest() {
    alert("scripts loaded");
}
function getQueryStringParameter(paramToRetrieve) {
    var params = document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}

function execCSOMTitleRequest() {
    var context;
    var factory;
    var appContextSite;
    var collList;

    var hostweburl = decodeURIComponent($.getUrlVar("SPHostUrl"));
    var appweburl = decodeURIComponent($.getUrlVar("SPAppWebUrl"));

    //Get the client context of the AppWebUrl
    context = new SP.ClientContext(appwebUrl);
    //Get the ProxyWebRequestExecutorFactory
    factory = new SP.ProxyWebRequestExecutorFactory(appwebUrl);
    //Assign the factory to the client context.
    context.set_webRequestExecutorFactory(factory);
    //Get the app context of the Host Web using the client context of the Application.
    appContextSite = new SP.AppContextSite(context, hostwebUrl);
    //Get the Web
    this.web = context.get_web();
    //Load Web.
    context.load(this.web);
    context.executeQueryAsync(
        Function.createDelegate(this, successTitleHandlerCSOM),
        Function.createDelegate(this, errorTitleHandlerCSOM)
        );
    //success Title
    function successTitleHandlerCSOM(data) {
        $('#lblResultTitle').html("<b>Via CSOM the title is:</b> " + this.web.get_title());
    }
    //Error Title
    function errorTitleHandlerCSOM(data, errorCode, errorMessage) {
        $('#lblResultLists').html("Could not complete CSOM call: " + errorMessage);
    }
}


function execCSOMListRequest() {
    var context;
    var factory;
    var appContextSite;
    var collList;

    var hostwebUrl = decodeURIComponent(
                        getQueryStringParameter("SPHostUrl")
    );
    var appwebUrl = decodeURIComponent(
                        getQueryStringParameter("SPAppWebUrl")
    );

    //Get the client context of the AppWebUrl
    context = new SP.ClientContext(appwebUrl);
    //Get the ProxyWebRequestExecutorFactory
    factory = new SP.ProxyWebRequestExecutorFactory(appwebUrl);
    //Assign the factory to the client context.
    context.set_webRequestExecutorFactory(factory);
    //Get the app context of the Host Web using the client context of the Application.
    appContextSite = new SP.AppContextSite(context, hostwebUrl);
    //Get the Web
    this.web = context.get_web();
    // Get the Web lists.
    collList = this.web.get_lists();
    //Load Lists.
    context.load(collList);
    context.executeQueryAsync(
        Function.createDelegate(this, successListHandlerCSOM),
        Function.createDelegate(this, errorListHandlerCSOM)
        );
    //Success Lists
    function successListHandlerCSOM() {
        var listEnumerator = collList.getEnumerator();
        $('#lblResultLists').html("<b>Via CSOM the lists are:</b><br/>");
        while (listEnumerator.moveNext()) {
            var oList = listEnumerator.get_current();
            $('#lblResultLists').append(oList.get_title() + " (" + oList.get_itemCount() + ")<br/>");
        }
    }
    //Error Lists
    function errorListHandlerCSOM(data, errorCode, errorMessage) {
        $('#lblResultLists').html("Could not complete CSOM Call: " + errorMessage);
    }
};

//***********************************************************************************************************

//$(document).ready(function () {
//    $.getScript(qsHostUrl + "/_layouts/15/SP.RequestExecutor.js", getHostInfo);

function getHostInfo() {

    var qsHostUrl = decodeURIComponent($.getUrlVar("SPHostUrl"));
    var qsAppUrl = decodeURIComponent($.getUrlVar("SPAppWebUrl"));

    var ctxApp = new SP.ClientContext(qsAppUrl);
    var factory = new SP.ProxyWebRequestExecutorFactory(qsAppUrl);
    ctxApp.set_webRequestExecutorFactory(factory);
    var ctxHost = new SP.AppContextSite(ctxApp, qsHostUrl);
    var web = ctxHost.get_web();
    ctxApp.load(web);

    ctxApp.executeQueryAsync(
        Function.createDelegate(this, getHostInfoSuccess),
        Function.createDelegate(this, getHostInfoError)
    );

    function getHostInfoSuccess(sender, args) {
        $('<div>' + 'Title1: ' + web.get_title() + '<br/>' +
        'Description: ' + web.get_description() + '</div>'
        ).appendTo('.TimeBookingContent');

        $('<div>' + 'Title2: ' + web.get_title() + '<br/>' +
         'Description: ' + web.get_description() + '</div>'
         ).appendTo($('.TimeBookingContent'));
    }
    function getHostInfoError(sender, args) {
        $('<div>' + 'Request Failed: ' + web.get_title() + '<br/>' +
        'Stacktrace: ' + web.get_description() + '</div>'
        ).appendTo('.TimeBookingContent');
    }
//    }
}