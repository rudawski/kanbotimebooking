﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>

    <script type="text/javascript" src="/_layouts/15/SP.RequestExecutor.js"></script>

    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Work Time Booking
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div id="divLeftPanel">
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
        <p>
            <asp:HyperLink runat="server" NavigateUrl="/OCKanBoTimeBooking/Lists/CustomerList/AllItems.aspx" 
                 Text="Customers" />
        </p>
        <p>
            <asp:HyperLink runat="server" NavigateUrl="/OCKanBoTimeBooking/Lists/TimeBookingList/AllItems.aspx" 
                 Text="Time Booking" />
        </p>
    </div>

    <div id="TimeBookingContent" class="TimeBookingContent">
        <div style="display:table;margin:10px">
            <div style="display:table-row">
                <div style="display:table-cell;float:left">
                    <div style="display:inline-table">
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lShortDescr" CssClass="label" EnableViewState="true" runat="server" Text="Short Descr:" />
                                <div>
                                    <asp:TextBox ID="tbShortDescr" CssClass="control" runat="server" MaxLength="250" TextMode="SingleLine" EnableViewState="true" CausesValidation="true" />
                                    <span class="isrequired" style="vertical-align:top">*</span>
                                    <div>
                                        <asp:RequiredFieldValidator ID="emptyShortDescrValidator" runat="server" ControlToValidate="tbShortDescr" Display="Dynamic"
                                                                    ErrorMessage="You must specify a value for this required field." EnableClientScript="true" SetFocusOnError="true" CssClass="validator" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lCustomer" CssClass="label" runat="server" EnableViewState="true" Text="Customer:" />
                                <div>
                                    <asp:DropDownList ID="ddlCustomer" Name="ddlCustomer" CssClass="control" runat="server" />
                                    <span class="isrequired">*</span>
                                    <div>
                                        <asp:CompareValidator ID="customerValidator" runat="server" ControlToValidate="ddlCustomer" ValueToCompare="0" Operator="GreaterThan"
                                                                SetFocusOnError="true" ErrorMessage="You must specify a value for this required field." Display="Dynamic" CssClass="validator" />
                                        <%--<asp:RequiredFieldValidator ID="customerValidator" runat="server" ControlToValidate="ddlCustomer" Display="Dynamic" ErrorMessage="You must specify a value for this required field."
                                                                    style="position:relative;" EnableClientScript="false" Text="Select Customer" InitialValue="0"/>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="display:table-cell;float:left">
                    <div style="display:inline-table">
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lBoard" CssClass="label" EnableViewState="true" runat="server" Text="Board:" />
                                <div>
                                    <asp:DropDownList ID="ddlBoard" CssClass="control" runat="server" AutoPostBack="true" >
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <span class="isrequired">*</span>
                                    <div>
                                        <%--<asp:CompareValidator ID="boardValidator" runat="server" ControlToValidate="ddlBoard" ValueToCompare="0" Operator="GreaterThan"
                                                        SetFocusOnError="true" ErrorMessage="You must specify a value for this required field." Display="Dynamic" CssClass="validator" />--%>
                                        <asp:CustomValidator ID="boardValidator" ValidateEmptyText="true" ErrorMessage="You must specify a value for this required field." Display="Dynamic" runat="server"
                                                    ControlToValidate="ddlBoard" ClientValidationFunction="validateSelectedBoard" EnableClientScript="true" CssClass="validator" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lCard" CssClass="label" EnableViewState="true" runat="server" Text="Card:" />
                                <div>
                                    <asp:DropDownList ID="ddlCard" CssClass="control" runat="server" >
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <span class="isrequired">*</span>
                                    <div>
                                        <asp:CustomValidator ID="cardValidator" ValidateEmptyText="true" ErrorMessage="You must specify a value for this required field." Display="Dynamic" runat="server"
                                                    ControlToValidate="ddlCard" ClientValidationFunction="validateSelectedCard" EnableClientScript="true" CssClass="validator" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lHours" CssClass="label" runat="server" Text="Work Hours:" />
                                <div>
                                    <asp:TextBox ID="tbHours" CssClass="control" runat="server" TextMode="SingleLine" EnableViewState="true" Text="" CausesValidation="true" />
                                    <span class="isrequired">*</span>
                                    <%--<asp:RequiredFieldValidator ID="emptyHoursValidator" runat="server" ControlToValidate="txtHours" Display="Dynamic" ErrorMessage="You must specify a value for this required field."
                                                                style="position:relative;" EnableClientScript="false"/>--%>
                                    <div>
                                        <asp:CustomValidator ID="hoursValidator" ValidateEmptyText="true" CssClass="validator" ErrorMessage="You must specify a value for this required field." Display="Dynamic" runat="server"
                                                            ControlToValidate="tbHours" ClientValidationFunction="validateNumeric" EnableClientScript="true" style="display:block" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display:table-row">
                            <div style="display:table-cell">
                                <asp:Label ID="lDay" CssClass="label" runat="server" Text="Work Day:" />
                                <div class="WorkDayControl">
                                    <%--<asp:Calendar ID="calCalendar" CssClass="control" runat="server" ></asp:Calendar>--%>
                                    <table>
                                        <row>
                                            <td>
                                                <SharePoint:DateTimeControl ID="dtDate" CssClassTextBox="control datetime" runat="server" DateOnly="true"
                                                                            ErrorMessage="You must specify a value for this required field." IsRequiredField="true" />
                                            </td>
                                            <td  style="vertical-align:top">
                                                <span class="isrequired">*</span>
                                            </td>
                                        </row>
                                    </table>
                                    <%--<input type="text" id="dtDatePicker" runat="server" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display:table-row">
                <div style="display:table-cell">
                    <asp:Label ID="lComments" CssClass="label" runat="server" Text="Comments:" />
                    <div>
                        <asp:TextBox ID="tbComments" CssClass="control" TextMode="MultiLine" Rows="9" Columns="130" runat="server" />
                    </div>
                </div>
            </div>
            <div style="display:table-row">
                <div style="display:table-cell; text-align:center; margin:15px">
                    <%--<asp:Button Text="Save" CssClass="pushbutton" ID="buttonSave" runat="server" BorderStyle="None" CausesValidation="true" OnClick="buttonSave_Click" />--%>
                    <%--<button title="Save" class="pushbutton" id="buttonOk" runat="server" onsubmit="" onclick="" causesvalidation="true">Save</button>--%>
                    <asp:Button Text="Save" CssClass="pushbutton" ID="buttonSave" runat="server" UseSubmitBehavior="false" CausesValidation="true" OnClientClick="javascript: OnButtonSaveClick(); return false;" />
                    <asp:Button Text="Clear" CssClass="pushbutton" ID="buttonClear" runat="server" UseSubmitBehavior="false" CausesValidation="false" OnClientClick="javascript: OnButtonCancelClick(); return false;" />
                </div>
            </div>
        </div>
    </div>

    <div id="divBookingList">
        <%--<SharePoint:ListView ListId="CustomerList" runat="server" />--%>
        <WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" 
            ID="full" Title="loc:full" >
            <WebPartPages:XsltListViewWebPart ID="XsltListViewWebPart2" 
                runat="server" ListUrl="/OCKanBoTimeBooking/Lists/TimeBookingList" IsIncluded="True" 
                NoDefaultStyle="TRUE" Title="Saved time booking records" PageType="PAGE_NORMALVIEW" 
                Default="False" ViewContentTypeId="0x"> 
            </WebPartPages:XsltListViewWebPart>
        </WebPartPages:WebPartZone>
    </div>
</asp:Content>
